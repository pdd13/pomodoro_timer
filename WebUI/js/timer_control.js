
var isTimerActive = false;
var timerStepInMilliseconds = 1000;

function TimerControl(timer)
{
    if (timer.Status == "Run") timer.Continue(); else
    if (isTimerActive == true) alert("Other timer is running!"); else
    {
        isTimerActive = true;

        timer.Start();
        UpdateTopStatus(timer);
        UpdateTopMassage(timer);
        UpdateOfButtonStart(timer, "Timer Continue")
        UpdateTableStatus(timer);
        UpdateTableRemain(timer);
        UpdateTablePassed(timer);
        FadeinTableRemain(timer);
        FadeinTablePassed(timer);

        var timeIntervalId = setInterval(function()
        {
            timer.TimeControl();

            if (timer.Status == "Run")
            {
                UpdateTitleName(timer);
                UpdateTableRemain(timer);
                UpdateTablePassed(timer);

                if (timer.RemainInMinutes == 0)
                {
                    if (timer.RemainInSeconds == 59) PlaySoundForLastMinute();
                    if (timer.RemainInSeconds == 1) PlaySoundForCompleted();
                }
            }
            else {
                clearInterval(timeIntervalId);

                UpdateTitleName(timer);
                UpdateTopStatus(timer);
                UpdateTopMassage(timer);
                UpdateTopCompleted(timer);
                UpdateTopInterrupted(timer);
                UpdateTopIntime(timer);
                UpdateTopLate(timer);
                UpdateOfButtonStart(timer, "Timer Start")

                UpdateTableStatus(timer);
                FadeoutTableRemain(timer);
                FadeoutTablePassed(timer);
                UpdateTableSum(timer);
                UpdateTablePercent();
                UpdateTableCompleted(timer);
                UpdateTableInterrupted(timer);
                UpdateTableIntime(timer);
                UpdateTableLate(timer);

                isTimerActive = false;
            }
        },
        timerStepInMilliseconds);
    }
}