
var isAutoContinueEnable = true;

function AutoContinueSwitch()
{
    if (isAutoContinueEnable) AutoContinueDisable();
    else AutoContinueEnable();
}
function AutoContinueEnable()
{
    isAutoContinueEnable = true;
    var i; for(i = 0; i < timersList.length; i++) timersList[i].IsAutoContinueEnable = isAutoContinueEnable;
    document.getElementById("navbar_button_continue").className = 'navbar_button_text col-md-1 navbar-btn navbar-right btn-info btn-sm';
    document.getElementById("navbar_button_continue").innerHTML = 'Auto Continue';
    document.getElementById("navbar_button_continue").style = "color: white";
}
function AutoContinueDisable()
{
    isAutoContinueEnable = false;
    var i; for(i = 0; i < timersList.length; i++) timersList[i].IsAutoContinueEnable = isAutoContinueEnable;
    document.getElementById("navbar_button_continue").className = 'navbar_button_text col-md-1 navbar-btn navbar-right btn btn-sm';
    document.getElementById("navbar_button_continue").innerHTML = 'Auto Continue';
    document.getElementById("navbar_button_continue").style = "color: #555555";
}