
var numberOfImages = 20; // number of .jpg files in 'image' folder
var indexOfImage = 0; // number from 0 to 'numberOfImages'
var nameOfImage = ''; // naming format: xxx (e.c. 001)
var pathToImage = ''; // path to .jpg file

function SetSelectImage()
{
    previousIndexOfImage = indexOfImage;
    indexOfImage = parseInt(document.querySelector('#navbar_form_index_image').value);
    document.querySelector('#navbar_form_index_image').value = "";

    if (indexOfImage <= numberOfImages)
    {
        GetPathOfImage();
        DisplayIndexOfImage();
        ReplaceBackground();
    }
    else {
        indexOfImage = previousIndexOfImage;
        alert("Set the value from 0 to " + numberOfImages + "!");
    }
}
function SetNextImage()
{
    if (indexOfImage == numberOfImages) indexOfImage = 0;
    else indexOfImage++;

    GetPathOfImage();
    DisplayIndexOfImage();
    ReplaceBackground();
}
function SetPreviousImage()
{
    if (indexOfImage == 0) indexOfImage = numberOfImages;
    else indexOfImage--;

    GetPathOfImage();
    DisplayIndexOfImage();
    ReplaceBackground();
}
function GetPathOfImage()
{
    if (indexOfImage <10) nameOfImage = '00' + indexOfImage;
    else if (indexOfImage <100) nameOfImage = '0' + indexOfImage;
    else nameOfImage = indexOfImage;

    pathToImage = 'url(WebUI/image/' + nameOfImage + '.jpg)';
    ReplaceBackground();
}
function DisplayIndexOfImage()
{
    document.getElementById("navbar_form_index_image").placeholder = indexOfImage;
};
function ReplaceBackground()
{
    document.body.style.background = pathToImage;
}
document.addEventListener("DOMContentLoaded", ReplaceBackground);