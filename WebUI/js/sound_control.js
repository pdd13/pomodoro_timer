
var isTimerSoundEnable = true;
var isLaunchingSoundEnable = true;

function SoundSwitch()
{
    if (isTimerSoundEnable) SoundDisable();
    else SoundEnable();
}
function SoundEnable()
{
    isTimerSoundEnable = true;
    document.getElementById("navbar_button_sound").className = 'navbar_button_text col-md-1 navbar-btn navbar-right btn-info btn-sm';
    document.getElementById("navbar_button_sound").innerHTML = 'Sound';
    document.getElementById("navbar_button_sound").style = "color: white";
}
function SoundDisable()
{
    isTimerSoundEnable = false;
    document.getElementById("navbar_button_sound").className = 'navbar_button_text col-md-1 navbar-btn navbar-right btn btn-sm';
    document.getElementById("navbar_button_sound").innerHTML = 'Sound';
    document.getElementById("navbar_button_sound").style = "color: #555555";
}
function PlaySound(nameOfSoundFile)
{
    if (isTimerSoundEnable)
    {
        var audio = new Audio();
        audio.src = 'WebUI/sound/' + nameOfSoundFile;
        audio.play();
    }
}
function PlaySoundForLaunching()
{
    if (isLaunchingSoundEnable) PlaySound('timer_launching.mp3');
}
function PlaySoundForSet(timer)
{
    if (timer.Status == "Run") PlaySound('timer_error.mp3');
}
function PlaySoundForStart(timer)
{
    if (isTimerActive == false || timer.Status == "Run") PlaySound('timer_start.mp3');
    else PlaySound('timer_error.mp3');
}
function PlaySoundForStop(timer)
{
    if (timer.GetContinueStatus()) PlaySoundForCompleted(timer);
    else PlaySoundForInterrupt(timer);
}
function PlaySoundForInterrupt(timer)
{
    if (timer.Status == "Run") PlaySound('timer_interrupt.mp3');
    else PlaySound('timer_error.mp3');
}
function PlaySoundForCompleted()
{
    PlaySound('timer_completed.mp3');
}
function PlaySoundForLastMinute()
{
    PlaySound('timer_last_minute.mp3');
}