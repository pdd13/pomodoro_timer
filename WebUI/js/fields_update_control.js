
function UpdateTitleName(timer)
{
    var field = "title_name";
    var content = "Timer";

    if (timer.Status == "Run")
    {
        var seconds = (timer.RemainInSeconds < 10 ? "0" : "") + timer.RemainInSeconds;
        var minutes = (timer.RemainInMinutes < 10 ? "0" : "") + timer.RemainInMinutes;
        content = "Timer " + minutes + ":" + seconds;
    }
    document.getElementById(field).innerHTML = content;
}
function UpdateOfButtonStart(timer, text)
{
    var field = "." + timer.Name + "_button_start";
    var content = text;
    $(field).text(content);
}
function UpdateTableSet(timer)
{
    var field = "." + timer.Name + "_table_set";
    var content = (timer.ValueInMinutes < 10 ? "0" : "") + + timer.ValueInMinutes + ":00";
    $(field).text(content);
}
function UpdateTopStatus(timer)
{
    var field = "." + timer.Name + "_top_status";
    var content = timer.Status;
    $(field).text(content);
}
function UpdateTableStatus(timer)
{
    var field = "." + timer.Name + "_table_status";
    var content = timer.Status;
    $(field).text(content);
}
function UpdateTopMassage(timer)
{
    var field = "." + timer.Name + "_top_massage";
    var content = timer.Status == "Run" ? timer.Massage : "Not Active";
    $(field).text(content);
}
function UpdateTablePassed(timer)
{
    var seconds = (timer.PassedInSeconds < 10 ? "0" : "") + timer.PassedInSeconds;
    var minutes = (timer.PassedInMinutes < 10 ? "0" : "") + timer.PassedInMinutes;

    var field = "." + timer.Name + "_table_passed";
    var content = minutes + ":" + seconds;
    $(field).text(content);
}
function FadeinTablePassed(timer)
{
    var field = "." + timer.Name + "_table_passed";
    $(field).fadeIn(2000);
}
function FadeoutTablePassed(timer)
{
    var field = "." + timer.Name + "_table_passed";
    $(field).fadeOut(2000);
}
function UpdateTableRemain(timer)
{
    var seconds = (timer.RemainInSeconds < 10 ? "0" : "") + timer.RemainInSeconds;
    var minutes = (timer.RemainInMinutes < 10 ? "0" : "") + timer.RemainInMinutes;

    var field = "." + timer.Name + "_table_remain";
    var content = minutes + ":" + seconds;
    $(field).text(content);
}
function FadeinTableRemain(timer)
{
    var field = "." + timer.Name + "_table_remain";
    $(field).fadeIn(2000);
}
function FadeoutTableRemain(timer)
{
    var field = "." + timer.Name + "_table_remain";
    $(field).fadeOut(2000);
}
function ClearTableRemain(timer)
{
    var field = "." + temp.Name + "_table_remain";
    $(field).text("");
}
function UpdateTableSum(timer)
{
    var minutes = (Math.floor(timer.SumInMinutes%60) < 10 ? "0" : "") + Math.floor(timer.SumInMinutes%60);
    var hours = (Math.floor(timer.SumInMinutes/60) < 10 ? "0" : "") + Math.floor(timer.SumInMinutes/60);

    var field = "." + timer.Name + "_table_sum";
    var content = hours + ":" + minutes + ":00";
    $(field).text(content);
}
function UpdateTablePercent()
{
    var i, j;
    for(i = 0; i < timersList.length; i++)
    {
        var sumInMinutesOfAllTimers = 0;
        for(j = 0; j < timersList.length; j++) sumInMinutesOfAllTimers += timersList[j].SumInMinutes;

        var sumInPercentOfThisTimer = 0;
        if(sumInMinutesOfAllTimers > 0)
            sumInPercentOfThisTimer = Math.floor(100 * timersList[i].SumInMinutes / sumInMinutesOfAllTimers);
        else
            sumInPercentOfThisTimer = 0;

        var field = "." + timersList[i].Name + "_table_per";
        var content = sumInPercentOfThisTimer + "%";
        $(field).text(content);
    }
}
function UpdateTopCompleted(timer)
{
    var field = "." + timer.Name + "_top_completed";
    var content = "Completed : " + timer.NumberOfCompleted;
    $(field).text(content);
}
function UpdateTableCompleted(timer)
{
    var field = "." + timer.Name + "_table_completed";
    var content = timer.NumberOfCompleted;
    $(field).text(content);
}
function UpdateTopInterrupted(timer)
{
    var field = "." + timer.Name + "_top_interrupted";
    var content = "Interrupted : " + timer.NumberOfInterrupted;
    $(field).text(content);
}
function UpdateTableInterrupted(timer)
{
    var field = "." + timer.Name + "_table_interrupted";
    var content = timer.NumberOfInterrupted;
    $(field).text(content);
}
function UpdateTopLate(timer)
{
    var field = "." + timer.Name + "_top_late";
    var content = "To be Late : " + timer.NumberOfCompleted;
    $(field).text(content);
}
function UpdateTableLate(timer)
{
    var field = "." + timer.Name + "_table_late";
    var content = timer.NumberOfCompleted;
    $(field).text(content);
}
function UpdateTopIntime(timer)
{
    var field = "." + timer.Name + "_top_intime";
    var content = "Be in Time : " + timer.NumberOfInterrupted;
    $(field).text(content);
}
function UpdateTableIntime(timer)
{
    var field = "." + timer.Name + "_table_intime";
    var content = timer.NumberOfInterrupted;
    $(field).text(content);
}