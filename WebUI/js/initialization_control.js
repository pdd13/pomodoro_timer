
function PageInitialization()
{
    var i;
    for(i = 0; i < timersList.length; i++)
    {
        UpdateTopStatus(timersList[i]);
        UpdateTopMassage(timersList[i]);
        UpdateTopCompleted(timersList[i]);
        UpdateTopInterrupted(timersList[i]);
        UpdateTopIntime(timersList[i]);
        UpdateTopLate(timersList[i]);

        UpdateTableStatus(timersList[i]);
        UpdateTableSet(timersList[i]);
        UpdateTableSum(timersList[i]);
        UpdateTablePercent();
        UpdateTableCompleted(timersList[i]);
        UpdateTableInterrupted(timersList[i]);
        UpdateTableIntime(timersList[i]);
        UpdateTableLate(timersList[i]);

        $("." + timersList[i].Name + "_table_remain").fadeOut(100);
        $("." + timersList[i].Name + "_table_passed").fadeOut(100);
    }
}
document.addEventListener("DOMContentLoaded", PageInitialization);