
var isClockEnable = false;
var clockID = null;

function ClockSwitch()
{
    if (isClockEnable) ClockDisable();
    else ClockEnable();
}
function ClockEnable()
{
    isClockEnable = true;
    clockID = setInterval(function()
    {
        var date = new Date();
        var options = {

            year: 'numeric',
            month: 'long',
            day: 'numeric',
            weekday: 'short',
            hour: 'numeric',
            minute: 'numeric',
            second: 'numeric',
            hour12: false};

        document.getElementById("navbar_button_time").className = 'navbar_button_text col-md-2 navbar-btn navbar-right btn-warning btn-sm';
        document.getElementById("navbar_button_time").innerHTML = date.toLocaleString("en-US", options);
        document.getElementById("navbar_button_time").style = "color: #222222";
    }, 1000);
}
function ClockDisable()
{
    isClockEnable = false;
    clearInterval(clockID);
    document.getElementById("navbar_button_time").className = 'navbar_button_text col-md-2 navbar-btn navbar-right btn btn-sm';
    document.getElementById("navbar_button_time").innerHTML = 'Clock';
    document.getElementById("navbar_button_time").style = "color: #555555";
}