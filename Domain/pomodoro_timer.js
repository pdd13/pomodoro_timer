function makeTimer()
{
    var timer =
    {
        Name : "",
        Status : "Idle",
        Massage : "",
        ValueInMinutes : 3,
        PassedInSeconds : 0,
        PassedInMinutes : 0,
        RemainInSeconds : 0,
        RemainInMinutes : 0,
        SumInMinutes : 0,
        NumberOfCompleted : 0,
        NumberOfInterrupted : 0,
        IsAutoContinueEnable : true,
        IsContinueActive : false,
        ContinueInMinutes : 5,
        NameOfStatusForCompleted : "Completed",
        NameOfStatusForInterrupted : "Interrupted",

        Set : function()
        {
            if (this.Status != "Run")
            {
                var value = parseInt(prompt("Set the timer value (in minutes)"));
                if(value > 0) this.ValueInMinutes = value;
                else alert("The value of timer must be positive!");
            }
            else alert("The timer is running!");
        },
        Start : function()
        {
            if (this.Status != "Run")
            {
                this.Status = "Run";
                this.PassedInSeconds = 0;
                this.PassedInMinutes = 0;
                this.RemainInSeconds = 0;
                this.RemainInMinutes = this.ValueInMinutes;
            }
            else alert("The timer is running!");
        },
        Continue : function()
        {
            if (this.Status == "Run")
            {
                this.RemainInMinutes += this.ContinueInMinutes;
                this.ContinueActive = true;
            }
        },
        Stop : function()
        {
            if (this.GetContinueStatus()) this.Completed();
            else {
                if (this.Status == "Run") this.Interrupted();
                else alert("The timer is not running!");
            }
        },
        Completed : function()
        {
            this.Status = this.NameOfStatusForCompleted;
            this.NumberOfCompleted++;
            this.ContinueActive = false;
            this.SumUpdate();

            this.PassedInSeconds = 0;
            this.PassedInMinutes = 0;
            this.RemainInSeconds = 0;
            this.RemainInMinutes = 0;
        },
        Interrupted : function()
        {
            this.Status = this.NameOfStatusForInterrupted;
            this.NumberOfInterrupted++;
            this.SumUpdate();

            this.PassedInSeconds = 0;
            this.PassedInMinutes = 0;
            this.RemainInSeconds = 0;
            this.RemainInMinutes = 0;
        },
        TimeControl : function()
        {
            if (this.Status == "Run")
            {
                if (this.RemainInSeconds == 0)
                {
                    this.RemainInSeconds = 59;
                    if (this.RemainInMinutes > 0) this.RemainInMinutes--;
                }
                else this.RemainInSeconds--;

                if (this.PassedInSeconds == 59)
                {
                    this.PassedInSeconds = 0;
                    this.PassedInMinutes++;
                }
                else this.PassedInSeconds++;

                if (this.RemainInSeconds == 0 && this.RemainInMinutes == 0)
                {
                    if (this.IsAutoContinueEnable) this.Continue();
                    else this.Completed();
                }
            }
        },
        SumUpdate : function()
        {
            this.SumInMinutes += this.PassedInMinutes;
        },
        GetContinueStatus : function()
        {
            var continueStatus = false;
            continueStatus = this.ContinueActive && (this.PassedInMinutes >= this.ValueInMinutes);
            return continueStatus;
        }
    }

    return timer;
}